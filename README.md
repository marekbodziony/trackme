# TrackMe #

"TrackMe" is an Android app, which can track position of device and save it in device SQLite database. 

### Main functions: ###

* check actual device location (need GPS)
* show latitude and longitude
* show address (if data transmission is enable)
* save actual location (latitude, longitude, address, time, note) in SQLite database
* display saved positions on a list
* show details of saved location when it's clicked on the list
* list can be sorted by date or by street name
* delete saved location (from details view or by swipe on the list)