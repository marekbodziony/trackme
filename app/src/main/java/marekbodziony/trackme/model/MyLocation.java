package marekbodziony.trackme.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.GregorianCalendar;


public class MyLocation implements Parcelable{

    private long id = -1;
    private String address;
    private float lat;
    private float lon;
    private GregorianCalendar dateOfSaving;
    private String note = "";

    // constructor
    public MyLocation (String address, float lat, float lon, String note){
        this.address = address;
        this.lat = lat;
        this.lon = lon;
        dateOfSaving = new GregorianCalendar();
        this.note = note;
    }

    // getters
    public long getId() {return id;}
    public String getAddress() {return address;}
    public float getLat() {return lat;}
    public float getLon() {return lon;}
    public GregorianCalendar getDateOfSaving() {return dateOfSaving;}
    public String getNote() {return note;}

    // setters
    public void setId(long id) {this.id = id;}
    public void setAddress(String address) {this.address = address;}
    public void setNote(String note) {this.note = note;}
    public void setDateOfSaving(long milis) {dateOfSaving.setTimeInMillis(milis);}


    // ---------------- for sending MyLocation object in Intent ------------------------------ //

    // constructor
    public MyLocation (Parcel in){
        id = in.readLong();
        address = in.readString();
        lat = in.readFloat();
        lon = in.readFloat();
        dateOfSaving = new GregorianCalendar();
        dateOfSaving.setTimeInMillis(in.readLong());
        note = in.readString();
    }
    @Override
    public int describeContents() {return 0;}

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(address);
        dest.writeFloat(lat);
        dest.writeFloat(lon);
        dest.writeLong(dateOfSaving.getTimeInMillis());
        dest.writeString(note);
    }

    // Parcelable creator
    public static final Parcelable.Creator<MyLocation> CREATOR = new Parcelable.Creator<MyLocation>(){

        @Override
        public MyLocation createFromParcel(Parcel source) {
            return new MyLocation(source);
        }

        @Override
        public MyLocation[] newArray(int size) {
            return new MyLocation[size];
        }
    };
    // ---------------------------------------------------------------------------------------- //
}
