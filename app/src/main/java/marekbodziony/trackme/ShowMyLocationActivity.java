package marekbodziony.trackme;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import marekbodziony.trackme.model.MyLocation;
import marekbodziony.trackme.utils.DatabaseHelper;
import marekbodziony.trackme.utils.Lib;

import static marekbodziony.trackme.utils.Lib.*;

public class ShowMyLocationActivity extends AppCompatActivity implements OnMapReadyCallback{

    private TextView address;
    private TextView lat;
    private TextView lon;
    private TextView date;
    private TextView note;
    private ImageView delete;

    private DatabaseHelper db;

    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_my_location);

        address = (TextView) findViewById(R.id.save_address_val);
        lat = (TextView) findViewById(R.id.save_lat_val);
        lon = (TextView) findViewById(R.id.show_lon_val);
        date = (TextView) findViewById(R.id.show_date_val);
        note = (TextView) findViewById(R.id.show_note_val);
        delete = (ImageView) findViewById(R.id.show_delete_img);

        getLocationFromIntentAndDisplay(getIntent());   // get MyLocation object from Intent and display

        db = new DatabaseHelper(this);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askToDelete();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.show_map_fragment);
        mapFragment.getMapAsync(this);
    }

    // method gets MyLocation object from Intent and displays it
    private void getLocationFromIntentAndDisplay(Intent i){
        MyLocation loc = (MyLocation)i.getParcelableExtra(Lib.INTENT_LOCATION);
        address.setText(loc.getAddress());
        lat.setText(Float.toString(loc.getLat()));
        lon.setText(Float.toString(loc.getLon()));
        date.setText(loc.getDateOfSaving().getTime().toString());
        note.setText(loc.getNote());
        Log.d(TAG,"OK! Item id = " + loc.getId());
    }

    // show dialog asking if location sould be deleted
    public void askToDelete (){
        new AlertDialog.Builder(this).setTitle("Are you sure to delete?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent delete = new Intent(getApplicationContext(), MainActivity.class);
                        delete.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        db.deleteLocation((MyLocation)getIntent().getParcelableExtra(Lib.INTENT_LOCATION));
                        startActivity(delete);
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                }).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        // Add a marker in Sydney, Australia, and move the camera.
        LatLng loc = new LatLng(Double.parseDouble(lat.getText().toString()), Double.parseDouble(lon.getText().toString()));
        map.addMarker(new MarkerOptions().position(loc).title(address.getText().toString()));
        map.moveCamera(CameraUpdateFactory.newLatLng(loc));
    }
}
