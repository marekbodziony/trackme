package marekbodziony.trackme;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;
import android.widget.Toast;

import static marekbodziony.trackme.utils.Lib.*;

public class WelcomeActivity extends AppCompatActivity {

    private ConstraintLayout layout;
    private TextView tapText;

    private boolean locationPermissionGranted = false;
    private boolean startApplication = false;
    private Intent startAppIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        // check for ACCESS_FINE_LOCATION permission
        askForFineLocationPermission();

        startAppIntent = new Intent(this, MainActivity.class);

        // set animated text - "tap to start"
        tapText = (TextView) findViewById(R.id.welcome_tap_txt);
        Animation tapAnim = new AlphaAnimation(0.1f,1.0f);
        tapAnim.setDuration(250);
        tapAnim.setStartOffset(20);
        tapAnim.setRepeatMode(Animation.REVERSE);
        tapAnim.setRepeatCount(Animation.INFINITE);
        tapText.setAnimation(tapAnim);

        // allow user to tap anywhere to start app (when permission is granted)
        layout = (ConstraintLayout) findViewById(R.id.welcome_layout);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startApplication = true;
                if (locationPermissionGranted) {
                    Log.d(TAG,"OK! Application started");
                    startActivity(startAppIntent);
                }
                else askForFineLocationPermission();
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PERMISSION_FINE_LOCATION_REQUEST : {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Log.d(TAG,"OK! Permission 'ACCESS_FINE_LOCATION' granted");
                    locationPermissionGranted = true;
                    // permission granted, app can start when user taps
                    if (startApplication) startActivity(startAppIntent);
                }else {
                    Log.d(TAG,"Error! Permission 'ACCESS_FINE_LOCATION' not granted.");
                    // permission is not granted, show Toast message
                    Toast.makeText(getApplicationContext(), R.string.need_loc_permission,Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    // this method checks if ACCESS_FINE_LOCATION permission is granted
    private void askForFineLocationPermission(){
        // if permission isn't granted show dialog prompt with question to grant ACCESS_FINE_LOCATION permission
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_FINE_LOCATION_REQUEST);
        }
        // if permission is granted set locationPermissionGranted=true
        else {
            locationPermissionGranted = true;
        }
    }
}
