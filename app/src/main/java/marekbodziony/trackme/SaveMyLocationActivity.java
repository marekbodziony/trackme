package marekbodziony.trackme;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import marekbodziony.trackme.model.MyLocation;
import marekbodziony.trackme.utils.DatabaseHelper;

import static marekbodziony.trackme.utils.Lib.*;

public class SaveMyLocationActivity extends AppCompatActivity implements OnMapReadyCallback{

    private TextView lat;
    private TextView lon;
    private TextView adr;
    private EditText note;
    private ImageButton save;

    private DatabaseHelper db;

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_my_location);

        lat = (TextView) findViewById(R.id.save_lat_val);
        lon = (TextView) findViewById(R.id.save_lon_val);
        adr = (TextView) findViewById(R.id.save_address_val);
        note = (EditText) findViewById(R.id.save_note_edit);
        save = (ImageButton) findViewById(R.id.save_btn);

        Intent i = getIntent();
        final String latitude = i.getStringExtra(LAT);
        final String longitude = i.getStringExtra(LON);
        final String address =  getAddressFromLatLon(latitude,longitude);       // gets address information from position (lat and lon)
        lat.setText(latitude);
        lon.setText(longitude);
        adr.setText(address);

        // click to save
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyLocation loc = new MyLocation(address,Float.parseFloat(latitude),Float.parseFloat(longitude),note.getText().toString());
                db = new DatabaseHelper(getApplicationContext());
                db.addLocation(loc);
                Toast.makeText(getApplicationContext(),"saved", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                int sortBy = getIntent().getIntExtra(SORT_BY_BUNDLE,0);         //  get sort_by type from Intent
                i.putExtra(SORT_BY_BUNDLE,sortBy);                              //  set sort_by type in Intent (to call it back in main activity)
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

        SupportMapFragment supportMapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.save_map_fragment);
        supportMapFragment.getMapAsync(this);
    }

    // gets address information from position (lat and lon)
    private String getAddressFromLatLon(String latitude, String longitude){
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String address = "";
        double lat = Double.parseDouble(latitude);
        double lon = Double.parseDouble(longitude);
        try {
            List<Address> geoData = geocoder.getFromLocation(lat,lon,1);
            if (geoData != null){
                address = geoData.get(0).getAddressLine(0);
//                Log.d(TAG,"Address = " + address);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng myLoc = new LatLng(Double.parseDouble(lat.getText().toString()), Double.parseDouble(lon.getText().toString()));
        mMap.addMarker(new MarkerOptions().position(myLoc).title(adr.getText().toString()));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(myLoc));
    }
}
