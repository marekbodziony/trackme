package marekbodziony.trackme;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import marekbodziony.trackme.model.MyLocation;
import marekbodziony.trackme.utils.DatabaseHelper;
import static marekbodziony.trackme.utils.Lib.*;
import marekbodziony.trackme.utils.MyLocationAdapter;

public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener{

    private DatabaseHelper db;

    private TextView lat;
    private TextView lon;
    private RecyclerView locRecycler;
    private Spinner sortSpinner;
    private ImageView showOnMap;

    private MyLocationAdapter adapter;
    private List<MyLocation> locList;

    private GoogleApiClient apiClient;
    private LocationRequest locationReq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lat = (TextView) findViewById(R.id.main_lat_val);
        lon = (TextView) findViewById(R.id.main_lon_val);
        showOnMap = (ImageView) findViewById(R.id.main_show_map_img);
        locRecycler = (RecyclerView) findViewById(R.id.main_list_recycler);
        sortSpinner = (Spinner) findViewById(R.id.main_sort_list_spinner);

        lat.setText(LAT_LON_DEFAULT);
        lon.setText(LAT_LON_DEFAULT);

        // show my location
        showOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // show my location only if there is data to show (latitude and longitude)
                if (!lat.getText().toString().contains(LAT_LON_DEFAULT)){
                    Intent myLoc = new Intent(getApplicationContext(),SaveMyLocationActivity.class);
                    myLoc.putExtra(LAT,lat.getText());
                    myLoc.putExtra(LON,lon.getText());
                    myLoc.putExtra(SORT_BY_BUNDLE,sortSpinner.getSelectedItemPosition());   //  set sort_by type in Intent (to call it back when back from save activity, while there's problem to get it from Bundle)
                    startActivity(myLoc);
                }else{
                    Toast.makeText(getApplicationContext(),"Check your GPS",Toast.LENGTH_SHORT).show();
                }
            }
        });

        db = new DatabaseHelper(this);
        locList = getDataFromSQLiteDB(db);

        // display location list
        adapter = new MyLocationAdapter(this, locList);
        locRecycler.setLayoutManager(new LinearLayoutManager(this));
        locRecycler.setHasFixedSize(true);
        locRecycler.setAdapter(adapter);
        setSwipeToDeleteOnRecycler();

        // sort location list
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,R.array.sort,R.layout.support_simple_spinner_dropdown_item);
        sortSpinner.setAdapter(spinnerAdapter);
        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sortList(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // create Google Play Services client
        apiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        apiClient.connect();            // connect the client to Google Play Services (Location services)
    }

    @Override
    protected void onStop() {
        apiClient.disconnect();         // disconnect the client to Google Play Services (Location services)
        super.onStop();
    }

    @Override
    protected void onPostResume() {
        // get sort_by type from Intent (when came from save my location, there's problem to get it from Bundle)
        int i = getIntent().getIntExtra(SORT_BY_BUNDLE,0);
        if (i == 1) sortSpinner.setSelection(i);
        super.onPostResume();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationReq = LocationRequest.create();
        locationReq.setInterval(5000);
        locationReq.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        askForLocationUpdateAndIfNeedForPermission();        // ask for location update and if needed ask for ACCESS_FINE_LOCATION permission
    }


    private void askForLocationUpdateAndIfNeedForPermission(){
        // if permission isn't granted show dialog prompt with question to give permission
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_FINE_LOCATION_REQUEST);
        }
        // if permission is granted ask for location update
        else {
            LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, locationReq, this);
//            Log.d(TAG, "OK! Permission granted, ask for location update.");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // if permission is granted, app can work
        if (requestCode == PERMISSION_FINE_LOCATION_REQUEST && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
//            Log.d(TAG,"OK! Permission 'ACCESS_FINE_LOCATION' granted");
        }
        // if permission isn't granted, app can't work - show Toast message and finish this activity (show welcome activity)
        else{
            Toast.makeText(getApplicationContext(), R.string.need_loc_permission,Toast.LENGTH_LONG).show();
            Log.d(TAG,"Error! Permission 'ACCESS_FINE_LOCATION' not granted.");
            finish();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        String latitude = Double.toString(location.getLatitude());
        String longitude = Double.toString(location.getLongitude());
        lat.setText(latitude);
        lon.setText(longitude);
        Log.d(TAG,"OK! LocationUpdate : lat = " + latitude + ", lon = " + longitude);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG,"Error! Connection to Google Play Services client is suspended!");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG,"Error! Connection to Google Play Services client failed!");
    }

    // method checks if GPS is enable in device
    private boolean isGpsEnable(){
        LocationManager locManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) return true;
        else return false;
    }

    // get all data from SQLite DB and return as List<MyLocation>
    private List<MyLocation> getDataFromSQLiteDB(DatabaseHelper db){
        List<MyLocation> list = db.getAllLocations();
        Log.d(TAG,"OK! Items fetched from DB : " + list.size());
        return list;
    }

    // delete MyLocation object from SQLite DB when swipe it on list
    private  void setSwipeToDeleteOnRecycler() {
        ItemTouchHelper.SimpleCallback touchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int pos = viewHolder.getLayoutPosition();
                MyLocation loc = locList.get(pos);
                db.deleteLocation(loc);
                Toast.makeText(getApplicationContext(),"deleted",Toast.LENGTH_SHORT).show();
            }
        };
        new ItemTouchHelper(touchCallback).attachToRecyclerView(locRecycler);
    }

    // sort list depending on spinner value
    private void sortList(int i){
        if (i == 0){
            locList = sortLocationListByDate(locList);
            adapter.notifyDataSetChanged();
        }
        else if (i == 1){
            locList = sortLocationListByStreet(locList);
            adapter.notifyDataSetChanged();
        }
    }
    // sort location list "by date"
    private List<MyLocation> sortLocationListByDate(List<MyLocation> list){    ;
        Collections.sort(list, new Comparator<MyLocation>() {
            @Override
            public int compare(MyLocation loc1, MyLocation loc2) {
                long loc1Date = loc1.getDateOfSaving().getTimeInMillis();
                long loc2Date = loc2.getDateOfSaving().getTimeInMillis();
                if (loc1Date < loc2Date) return 1;
                else if (loc1Date > loc2Date) return -1;
                else return 0;
            }
        });
        return  list;
    }
    // sort location list "by street"
    private List<MyLocation> sortLocationListByStreet(List<MyLocation> list){    ;
        Collections.sort(list, new Comparator<MyLocation>() {
            @Override
            public int compare(MyLocation loc1, MyLocation loc2) {
                return loc1.getAddress().compareTo(loc2.getAddress());
            }
        });
        return  list;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save 'sort by' type in Bundle
        outState.putInt(SORT_BY_BUNDLE,sortSpinner.getSelectedItemPosition());
        Log.d(TAG,"Bundle | Spinner id=" + outState.getInt(SORT_BY_BUNDLE));

    }
}
