package marekbodziony.trackme.utils;

/**
 *  In this class all static final values will be stored
 *  Every activity can use this data
 */

public class Lib {

    public static final String TAG = "LogTrack";
    public static final int PERMISSION_FINE_LOCATION_REQUEST = 1;

    public static final String LAT = "Latitude";
    public static final String LON = "Longitude";

    public static final String INTENT_LOCATION = "location";
    public static final String INTENT_SAVE = "save";

    public static final String DB_NAME = "trackme.db";
    public static final int DB_VERSION = 2;

    public static final String TABLE_LOCATIONS = "locations";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_LAT = "lat";
    public static final String COLUMN_LON = "lon";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_NOTE = "note";

    public static final String[] ALL_COLUMNS = {COLUMN_ID, COLUMN_ADDRESS, COLUMN_LAT, COLUMN_LON, COLUMN_DATE, COLUMN_NOTE};

    public static final String CREATE_LOCATIONS_TABLE = "CREATE TABLE " + TABLE_LOCATIONS + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_ADDRESS + " TEXT NOT NULL, " + COLUMN_LAT + " REAL NOT NULL, " + COLUMN_LON + " REAL NOT NULL, " + COLUMN_DATE + " INTEGER NOT NULL, "
            + COLUMN_NOTE + " TEXT);";
    public static final String DROP_LOCATIONS_TABLE = "DROP TABLE IF EXISTS " + TABLE_LOCATIONS;

    public static final String LAT_LON_DEFAULT = "----";

    public static final String SORT_BY_BUNDLE = "sort by";
}
