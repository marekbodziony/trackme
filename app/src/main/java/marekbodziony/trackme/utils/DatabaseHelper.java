package marekbodziony.trackme.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import marekbodziony.trackme.model.MyLocation;
import static marekbodziony.trackme.utils.Lib.*;


public class DatabaseHelper extends SQLiteOpenHelper{


    public DatabaseHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_LOCATIONS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // drop older version if exists
        db.execSQL(DROP_LOCATIONS_TABLE);
        onCreate(db);
    }

    // add location to database
    public void addLocation (MyLocation loc){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ADDRESS,loc.getAddress());
        values.put(COLUMN_LAT,loc.getLat());
        values.put(COLUMN_LON,loc.getLon());
        values.put(COLUMN_DATE,loc.getDateOfSaving().getTimeInMillis());
        values.put(COLUMN_NOTE,loc.getNote());

        db.insert(TABLE_LOCATIONS,null,values);
        Log.d(TAG,"Location '" + loc.getAddress() + "' added to DB");
    }

    // delete location from database
    public void deleteLocation (MyLocation loc){
        SQLiteDatabase db = this.getWritableDatabase();
        long id = loc.getId();
        db.delete(TABLE_LOCATIONS, COLUMN_ID + "=" + id, null);
        Log.d(TAG,"Location '" + loc.getAddress() + "' deleted from DB");
    }

    public List<MyLocation> getAllLocations(){
        SQLiteDatabase db = this.getWritableDatabase();
        List<MyLocation> list = new ArrayList<>();
        Cursor cursor = db.query(TABLE_LOCATIONS, ALL_COLUMNS, null, null, null, null, null);

        if (cursor.moveToFirst()){
            while (cursor.moveToNext()){
                MyLocation loc;
                String address, note;
                float lat, lon;
                long id, date;
                id = cursor.getLong(0);
                address = cursor.getString(1);
                lat = cursor.getFloat(2);
                lon = cursor.getFloat(3);
                date = cursor.getLong(4);
                note = cursor.getString(5);
                loc = new MyLocation(address,lat,lon,note);
                loc.setId(id);
                loc.setDateOfSaving(date);
                list.add(loc);
            }
        }
        cursor.close();
        return list;
    }

}
