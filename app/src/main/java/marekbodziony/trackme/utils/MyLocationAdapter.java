package marekbodziony.trackme.utils;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import marekbodziony.trackme.R;
import marekbodziony.trackme.ShowMyLocationActivity;
import marekbodziony.trackme.model.MyLocation;
import static marekbodziony.trackme.utils.Lib.*;

public class MyLocationAdapter extends RecyclerView.Adapter<MyLocationAdapter.LocationViewHolder> {

    private Context context;
    private List<MyLocation> list;



    // constructor
    public MyLocationAdapter (Context context, List<MyLocation> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.location_item,parent,false);
        LocationViewHolder holder = new LocationViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(LocationViewHolder holder, final int position) {
        MyLocation loc = list.get(position);
        holder.address.setText(loc.getAddress());
        holder.latAndLon.setText("Lat=" + loc.getLat() + ", Lon=" + loc.getLon());
        holder.date.setText(new SimpleDateFormat("yyyy-MM-dd").format(loc.getDateOfSaving().getTime()));
        holder.time.setText(new SimpleDateFormat("hh:mm aaa").format(loc.getDateOfSaving().getTime()));
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ShowMyLocationActivity.class).putExtra(INTENT_LOCATION,list.get(position)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class LocationViewHolder extends RecyclerView.ViewHolder{

        private TextView address;
        private TextView latAndLon;
        private TextView date;
        private TextView time;
        private View view;

        public LocationViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            address = (TextView) itemView.findViewById(R.id.loc_item_address);
            latAndLon = (TextView) itemView.findViewById(R.id.loc_item_lat_lon);
            date = (TextView) itemView.findViewById(R.id.loc_item_date);
            time = (TextView) itemView.findViewById(R.id.loc_item_time);
        }
    }
}
